//= masonry.pkgd.min.js
//= scrollbar/jquery.mCustomScrollbar.concat.min.js
//= jquery-ui-1.12.1.custom/jquery-ui.min.js
//= jquery-ui-1.12.1.custom/localization.js
//= jquery.fancyapps/jquery.fancybox.min.js
//= note.js
//= masked.js
window.addEventListener('load', function () {
    var loader = $('.loader');
    loader.fadeOut('slow');
    setTimeout(function () {
        loader.remove();
    }, 2000);
});


$(document).ready(function () {
    "use strict";

    // By Chris Coyier & tweaked by Mathias Bynens

    function fluidVideo($element) {

        // Find all YouTube videos
        var $allVideos = $("iframe[src^='https://www.youtube.com']"),

            // The element that is fluid width
            $fluidEl = $element || $("body");

        // Figure out and save aspect ratio for each video
        $allVideos.each(function () {

            $(this)
                .data('aspectRatio', this.height / this.width)

                // and remove the hard coded width/height
                .removeAttr('height')
                .removeAttr('width');

        });

        // When the window is resized
        // (You'll probably want to debounce this)
        $(window).resize(function () {

            var newWidth = $fluidEl.width();

            // Resize all videos according to their own aspect ratio
            $allVideos.each(function () {

                var $el = $(this);
                $el
                    .width(newWidth)
                    .height(newWidth * $el.data('aspectRatio'));
            });

            // Kick off one resize to fix all videos on page load
        }).resize();
    }

    Foundation.Drilldown.defaults.backButton = '<li class="js-drilldown-back"><a tabindex="0">Назад</a></li>';
    Foundation.Drilldown.defaults.parentLink = true;

    // Drilldown.defaults.backButton = '<li class="js-drilldown-back"><a tabindex="0">Назад</a></li>';
    // Foundation.drilldown.defaults.parentLink = true;
    // Foundation.OffCanvas.defaults.forceTop = false;
    $(document).foundation();

    $(document)
        .on('opened.zf.offcanvas', function () {
            $('html').addClass('is-locked');
        })
        .on('closed.zf.offcanvas', function () {
            $('html').removeClass('is-locked');
        });

    function swipeInit() {
        if (Foundation.MediaQuery.atLeast('large')) {
            $(document)
                .off('swipeleft', closeMainMenu)
                .off('swiperight', openMainMenu);
        } else {
            $(document)
                .on('swipeleft', closeMainMenu)
                .on('swiperight', openMainMenu);
        }
    };

    function openMainMenu() {
        $('#offCanvasMain').foundation('open');
    }

    function closeMainMenu() {
        $('#offCanvasMain').foundation('close');
    }

    swipeInit();


    // $('.is-locked body').bind("touchmove", {}, function(event){
    //     event.preventDefault();
    //     alert('move');
    // });

    var searchHeader = $('#header-search'),
        header = $('.site-header'),
        header_height = header.height(),
        footerAccordionStack;

    window.addEventListener('scroll', function (e) {
        if (header_height < window.pageYOffset) {
            header.addClass('is-fixed');
        } else {
            header.removeClass('is-fixed');
        }
    });

    var mobileInit = function () {
        var footerAccordion = $('.footer-accordion');
        footerAccordionStack = [];

        footerAccordion.each(function () {
            footerAccordionStack.push(new Foundation.Accordion($(this), {allowAllClosed: true}));
        });
    };


    var mobileCheck = function () {
        if (Foundation.MediaQuery.current == 'small') {
            mobileInit();
        }
    };


    mobileCheck();

    var currentDevice = Foundation.MediaQuery.atLeast('large') ? 'large' : Foundation.MediaQuery.current;

    window.addEventListener('resize', function () {
        swipeInit();
        mobileCheck();
        currentDevice = Foundation.MediaQuery.atLeast('large') ? 'large' : Foundation.MediaQuery.current;
        slidersInit();
        console.log('resize');
    });

    var slidersIsInit;
    var goodsSliders = [],
        slidersStack = [];

    // sliders settings
    var goodsSlider = {
            className: '.js-goods-slider',
            option: {
                pager: false,
                slideWidth: 300,
                moveSlides: 1,
                infiniteLoop: false
            },
            'large': {
                minSlides: 4,
                maxSlides: 4
            },
            'medium': {
                minSlides: 3,
                maxSlides: 3
            },
            'small': {
                minSlides: 1,
                maxSlides: 1
            }
        },

        adviseSlider = {
            className: '.js-advise-slider',
            option: {
                pager: false,
                slideWidth: 300,
                moveSlides: 1,
                nextSelector: '.advise-next',
                prevSelector: '.advise-prev',
                infiniteLoop: false
            },
            'large': {
                minSlides: 3,
                maxSlides: 3
            },
            'medium': {
                minSlides: 3,
                maxSlides: 3
            },
            'small': {
                minSlides: 1,
                maxSlides: 1
            }
        },
        brandsSlider = {
            className: '.js-brand-slider',
            option: {
                pager: false,
                slideWidth: 250,
                moveSlides: 1,
                nextSelector: '.brand-next',
                prevSelector: '.brand-prev',
                infiniteLoop: false
            },
            'large': {
                minSlides: 5,
                maxSlides: 5
            },
            'medium': {
                minSlides: 3,
                maxSlides: 3
            },
            'small': {
                minSlides: 1,
                maxSlides: 1
            }
        },
        relatedSlider = {
            className: '.js-related-slider',
            option: {
                pager: false,
                slideWidth: 300,
                moveSlides: 1,
                infiniteLoop: false
            },
            'large': {
                mode: 'vertical',
                minSlides: 2,
                maxSlides: 2
            },
            'medium': {
                mode: 'horizontal',
                minSlides: 3,
                maxSlides: 3
            },
            'small': {
                mode: 'horizontal',
                minSlides: 1,
                maxSlides: 1
            }

        };

    window.slidersInit = (function () {
        goodsSlider.option = $.extend({}, goodsSlider.option, goodsSlider[currentDevice]);
        brandsSlider.option = $.extend({}, brandsSlider.option, brandsSlider[currentDevice]);
        adviseSlider.option = $.extend({}, adviseSlider.option, adviseSlider[currentDevice]);
        relatedSlider.option = $.extend({}, relatedSlider.option, relatedSlider[currentDevice]);

        if (typeof slidersIsInit == 'undefined' || slidersIsInit == false) {
            $('.slider-main__items').bxSlider({
                minSlides: 1,
                maxSlides: 1,
                controls: false
            });

            if ($(goodsSlider.className).length > 0) {
                $(goodsSlider.className).each(function () {
                    goodsSliders.push($(this).bxSlider(goodsSlider.option));
                });
                goodsSliders.isInit = true;
            }

            if ($(relatedSlider.className).length > 0) {
                relatedSlider.bx = $(relatedSlider.className).bxSlider(relatedSlider.option);
                relatedSlider.isInit = true;
            }

            if ($('.js-brand-slider').length > 0) {
                brandsSlider.bx = $(brandsSlider.className).bxSlider(brandsSlider.option);
                brandsSlider.isInit = true;
            }

            if ($('.js-advise-slider').length > 0) {
                adviseSlider.bx = $('.js-advise-slider').bxSlider(adviseSlider.option);
                adviseSlider.isInit = true;
            }

            slidersIsInit = true;

        } else {
            // TODO: попытаться унифицировать перезагрузку слайдеров
            brandsSlider.isInit == true ? brandsSlider.bx.reloadSlider(brandsSlider.option) : '';
            relatedSlider.isInit == true ? relatedSlider.bx.reloadSlider(relatedSlider.option) : '';
            adviseSlider.isInit == true ? adviseSlider.bx.reloadSlider(adviseSlider.option) : '';
            goodsSliders.isInit == true ? goodsSliders.forEach(function (item) {
                    item.reloadSlider(goodsSlider.option);
                }) : '';
        }
    });

    slidersInit();

    var showAdd2Cart = (function () {
        var add2cartSlider = $('.js-add2cart-goods').bxSlider({
            pager: false,
            slideWidth: 300,
            moveSlides: 1,
            infiniteLoop: false,
            minSlides: 3,
            maxSlides: 3
        });
    });

    showAdd2Cart();


    // reload sliders after click on tabs
    $('#home-tabs').on('change.zf.tabs', function () {
        goodsSliders.forEach(function (item) {
            item.reloadSlider();
        });
    });

    searchHeader.on('focus', function () {
        $(this).closest('.site-header__search').addClass('is-active');
    }).on('blur', function () {
        $(this).closest('.site-header__search').removeClass('is-active');
    });

    $('.categories').masonry({
        columnWidth: '.category-sizer',
        itemSelector: '.category-tile',
        // percentPosition: true,
        gutter: 0
    });

    var categoryTiles = $('.category');
    categoryTiles.each(function () {
        var self = $(this),
            selfList = self.find('.category__sublist');
        if (typeof selfList !== 'undefined') {
            if (self.offset().top + self.outerHeight() < selfList.offset().top + selfList.outerHeight()) {
                self.find('.category__content').append('<a href=\'' + self.data('link') + '\' class=\'category__more\'>Показать еще</a>');
            }
        }
    });


    // product-page
    // TODO: переделать. Добавить поддержку ресайза
    var link_showreview = $('.js-show-review-tab');

    var productPageInit = function () {
        if ($('.product-page').length > 0) {
            if (currentDevice == 'small') {
                var productAccordion = $('.product-page__info');
                productAccordion.accorion = new Foundation.Accordion(productAccordion, {allowAllClosed: true});

                $('.product-page__info').on('up.zf.accordion', function () {
                    var self = this;
                    self.active = $(self).find('.is-active');
                    if (self.active.length > 0) {
                        console.log(self.active);
                        $('html,body').animate({scrollTop: self.active.offset().top - 100}, 'slow');
                    }
                });

                link_showreview.on('click', function () {
                    productAccordion.foundation('down', $('[data-link-target=\'reviews\']'));
                    $('html,body').animate({scrollTop: productAccordion.find('.is-active').offset().top - 100}, 'slow');
                });

            } else {
                var productTabs = $('.product-page__tabs');
                productTabs.tabs = new Foundation.Tabs(productTabs);

                link_showreview.on('click', function () {
                    productTabs.foundation('selectTab', $(this).data('target'));
                    $('html,body').animate({scrollTop: productTabs.find('.is-active').offset().top - 150}, 'slow');
                });
            }
        }

        $('.js-product-thumb').bxSlider({
            pagerCustom: '#product-page__thumbnails',
            controls: false
        });

        $('.js-thumbnails-slider').bxSlider({
            minSlides: 4,
            maxSlides: 8,
            moveSlides: 1,
            slideWidth: 74,
            slideMargin: 11,
            hideControlOnEnd: true,
            pager: false,
            infiniteLoop: false
        });
    };

    productPageInit();


    /* order user-type listener*/
    $('[name="user-type"]').on('change', function (e) {
        var legalFields = $('.js-legal-entity');
        if ($(this).val() == 'legal') {
            legalFields.fadeIn();
        } else {
            legalFields.fadeOut();
        }
    });

    /**
     *   smart filter */
    (function () {
        var smartFilter = $('.smart-filter');
        smartFilter.accordion = new Foundation.Accordion(smartFilter, {
            multiExpand: true,
            allowAllClosed: true
        });

        $('.js-full-list').on('click', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $(this).closest('.smart-filter__prop-group').toggleClass('is-full');
        });

        $('.js-full-filter').on('click', function (e) {
            e.preventDefault();
            e.stopPropagation();
            smartFilter.toggleClass('full-filter');
        });
    }());


    /* smart filter END*/
    $.mCustomScrollbar.defaults.scrollButtons.enable = true;

    // compare init

    window.compareComponent = (function () {
        var cm = {};
        cm.option = {};
        cm.viewport = $('.cm__viewport');
        cm.itemWidth = 0;

        cm.items = $('.cm__item');
        cm.itemsCount = $('.cm__table-header .cm__item').length;
        cm.fieldset = $('.cm__fieldset');
        cm.fieldsetItems = $('.cm__fieldset .cm__fieldname');
        cm.itemGroup = $('.cm__table-body .cm__field-group');
        cm.itemGroup_all = $('.cm__item-group');

        cm.fieldgroup = $('.cm__field-group');
        cm.fieldSets = $('.cm__fieldset .cm__fieldname');

        $(document).on('mouseover', '.cm__field-group, .cm__fieldset .cm__fieldname', function (e) {
            $(cm.fieldgroup[$(this).index()]).addClass('is-hover');
            $(cm.fieldSets[$(this).index()]).addClass('is-hover');
        });
        $(document).on('mouseleave', '.cm__field-group, .cm__fieldset .cm__fieldname', function (e) {
            $(cm.fieldgroup[$(this).index()]).removeClass('is-hover');
            $(cm.fieldSets[$(this).index()]).removeClass('is-hover');
        });

        $('.cm__scroll-bar').mCustomScrollbar({
            axis: "x",
            snapAmount: cm.itemWidth,
            theme: 'inset-dark',
            /* advanced:{
             updateOnContentResize: false,
             },*/
            callbacks: {
                onScroll: function () {
                    $('.cm__item-group').css({
                        '-ms-transform': 'translate(-' + this.mcs.left + 'px, 0);',
                        'transform': 'translate(' + this.mcs.left + 'px, 0)'
                    });
                }
            }
        });

        setItemWidth();

        // touch event
        $('#cm__table').on('swiperight', function (e) {
            e.stopPropagation();
            $('.cm__scroll-bar').mCustomScrollbar('scrollTo', '+=' + cm.itemWidth);
        });

        $('#cm__table').on('swipeleft', function (e) {
            e.stopPropagation();
            $('.cm__scroll-bar').mCustomScrollbar('scrollTo', '-=' + cm.itemWidth);
        });
        // touch event  END

        $(window).on('resize', setItemWidth);

        BX.addCustomEvent('onAjaxSuccess', BX.delegate(function(){
            compareComponent();
        }, this));

        $(window).on('scroll', function () {
            $('#cm__filter').removeClass('is-show');
        });

        function setItemWidth() {
            cm.visibleItemCount = Foundation.MediaQuery.atLeast('medium') ? 3 : 2;
            cm.itemWidth = Math.round(cm.viewport.width() / cm.visibleItemCount);
            cm.items.css('max-width', cm.itemWidth + 'px');
            $('.cm__scroll-bar-content').width(cm.itemWidth * cm.itemsCount);
            $('.cm__scroll-bar .mCSB_container').width(cm.itemWidth * cm.itemsCount);

            $('.cm__scroll-bar').mCustomScrollbar('update', {snapAmount: cm.itemWidth});
            // выравнивание высоты строк
            if (Foundation.MediaQuery.atLeast('large')) {
                cm.fieldsetItems.each(function (index, item) {
                    var h1 = $(cm.fieldsetItems[index]).outerHeight(),
                        h2 = $(cm.itemGroup[index]).outerHeight();
                    if (h1 >= h2) {
                        $(cm.itemGroup[index]).outerHeight(h1);
                    } else {
                        $(cm.fieldsetItems[index]).outerHeight(h2)
                    }
                });
            }
        }
    });

    compareComponent();
    // contact page
    $('.js-show-contacts-panel').on('click', function (e) {
        e.preventDefault();
        e.stopPropagation();

        $('.contact-page__info-content').toggleClass('is-hide');
    });

    // bitrix admin panel
    $('#bx-panel-expander, #bx-panel-hider').on('click', bxPanelTakeHeight);

    bxPanelTakeHeight();

    function bxPanelTakeHeight() {
        var bxPanel = $('#bx-panel');
        if (bxPanel.outerHeight() > 0) {
            $('.main-container').css('margin-top', bxPanel.outerHeight());
        }
    }

    $(window).on('open.zf.reveal', function () {
        var $browserWindowWidth = window.innerWidth;
        var $bodyWidth = document.body.clientWidth;
        var $winPadding = $browserWindowWidth - $bodyWidth;

        if ($browserWindowWidth !== $bodyWidth) {
            $('body, .site-header-wrapper').css('padding-right', $winPadding);
        }
    });

    $(window).on('closed.zf.reveal', function () {
        $('body, .site-header-wrapper').css('padding-right', '0');
    });

    $(window).on('scroll', function () {
        if ($(this).scrollTop() > window.innerHeight * 1.5) {
            $('.toTop').addClass('is-show');
        } else {
            $('.toTop').removeClass('is-show');
        }
    });

    $('.toTop').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 800);
        return false;
    });


    // masked input tel
    $('[type=tel]').mask('+7(999) 999-99-99');

});